<?php
// +----------------------------------------------------------------------
// | SparkShop 坚持做优秀的商城系统
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2099 http://sparkshop.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: NickBai
// +----------------------------------------------------------------------
namespace app\index\controller;

use app\service\OrderRefundService;
use app\service\UserOrderService;
use think\facade\View;

class UserOrder extends Base
{
    public function initialize()
    {
        parent::initialize();
        pcLoginCheck();
    }

    /**
     * 取消订单
     */
    public function cancel()
    {
        $id = input('param.id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->orderCancel($id, session('home_user_id'), session('home_user_name'));
        return json($res);
    }

    /**
     * 去支付
     */
    public function goPay()
    {
        $param = input('post.');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->goPay($param, session('home_user_id'));
        return json($res);
    }

    /**
     * 申请售后
     */
    public function refund()
    {
        // 处理提交售后
        if (request()->isPost()) {

            $param = input('post.');
            $postParam = $param;
            $param['order_num_data'] = json_decode($param['order_num_data'], true);
            $param['user_id'] = getFrontUserInfo()['id'];

            $userOrderService = new UserOrderService();
            $orderInfo = $userOrderService->refundTrail($param);
            $orderInfo['data']['refundConf']['only_refund'] =  explode("\n", $orderInfo['data']['refundConf']['only_refund']);
            $orderInfo['data']['refundConf']['goods_refund'] =  explode("\n", $orderInfo['data']['refundConf']['goods_refund']);
            $orderInfo['data']['postParam'] = $postParam;

            View::assign($orderInfo['data']);
        }

        return View::fetch();
    }

    /**
     * 处理申请售后
     */
    public function applyRefund()
    {
        $userOrderService = new UserOrderService();
        // 处理提交售后
        if (request()->isPost()) {

            $param = input('post.');
            $userInfo = getFrontUserInfo();
            unset($param['file']);

            if (is_numeric($param['order_detail_id'])) {
                $param['order_num_data'] = json_decode($param['order_num_data'], true);
                $newApplyNumData = [];
                foreach ($param['order_num_data'] as $vo) {
                    $newApplyNumData[] = ['order_detail_id' => $vo['order_detail_id'], 'num' => $param['refund_num']];
                }
                $param['order_num_data'] = json_encode($newApplyNumData);
            }

            if (isset($param['refund_img'])) {
                $param['refund_img'] = implode(',', $param['refund_img']);
            } else {
                $param['refund_img'] = '';
            }

            $res = $userOrderService->doRefundOrder($param, $userInfo);
            return json($res);
        }
    }

    /**
     * 退款进度
     */
    public function refundDetail()
    {
        $id = input('param.id');

        $orderRefundService = new OrderRefundService();
        $res = $orderRefundService->getRefundDetail($id, session('home_user_id'));
        if ($res['code'] != 0) {
            return build404($res);
        }
        if (!empty($res['data']['refund_img'])) {
            $res['data']['refund_img'] = explode(',', $res['data']['refund_img']);
        }
        View::assign($res['data']->toArray());

        $vipConf = getConfByType('shop_user_level');
        View::assign([
            'vipConf' => $vipConf
        ]);

        return View::fetch();
    }

    /**
     * 订单售后列表
     */
    public function afterOrder()
    {
        if (request()->isAjax()) {

            $param = input('param.');

            $orderRefundService = new OrderRefundService();
            $list = $orderRefundService->getRefundList($param, 2);

            return json($list);
        }

        return View::fetch();
    }

    /**
     * 取消退款
     */
    public function cancelRefund()
    {
        if (request()->isAjax()) {

            $id = input('param.id');

            $userOrderService = new UserOrderService();
            $res = $userOrderService->cancelRefund($id, session('home_user_id'));
            return json($res);
        }
    }

    /**
     * 物流信息
     */
    public function express()
    {
        $id = input('param.id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->getExpressInfo($id, session('home_user_id'));

        View::assign($res['data']);

        return View::fetch();
    }

    /**
     * 确认收货
     */
    public function received()
    {
        $orderId = input('param.id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->doReceived($orderId, session('home_user_id'), session('home_user_name'));
        if ($res['code'] == 0) {
            $res['msg'] = '操作成功';
        }

        return json($res);
    }

    /**
     * 快递信息查询
     */
    public function refundExpress()
    {
        if (request()->isPost()) {
            $param = input('post.');

            $orderRefundService = new OrderRefundService();
            $res = $orderRefundService->doRefundExpress($param, session('home_user_id'));
            if ($res['code'] == 0) {
                $res['msg'] = '操作成功';
            }

            return json($res);
        }

        $id = input('param.id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->getExpressInfo($id, session('home_user_id'));
        if ($res['code'] == 0) {
            $res['msg'] = '操作成功';
        }

        return json($res);
    }

    /**
     * 关闭订单
     */
    public function close()
    {
        $id = input('param.id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->closeOrder($id, session('home_user_id'), session('home_user_name'));
        if ($res['code'] == 0) {
            $res['msg'] = '操作成功';
        }

        return json($res);
    }

    /**
     * 评价
     */
    public function appraise()
    {
        if (request()->isPost()) {

            $param = input('post.');
            if (isset($param['pictures']) && !empty($param['pictures'])) {
                $param['pictures'] = implode(',', $param['pictures']);
            }

            $userOrderService = new UserOrderService();
            return json($userOrderService->doAppraise($param, getFrontUserInfo()));
        }

        $orderId = input('param.order_id');
        $orderDetailId = input('param.order_detail_id');

        $userOrderService = new UserOrderService();
        $res = $userOrderService->getGoodsComments($orderId, $orderDetailId, session('home_user_id'));

        if ($res['code'] != 0) {
            return build404($res);
        }

        if (!empty($res['data']['info']['comment']['pictures'])) {
            $res['data']['info']['comment']['pictures'] = explode(',', $res['data']['info']['comment']['pictures']);
        }

        View::assign($res['data']);

        return View::fetch();
    }
}